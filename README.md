<h1 align="center"><code> 🗄️:SN-DOKUS-1 </code></h1>
<h2 align="center"><i> Docusaurus Learning and testing  </i></h2>

---

1. [Wha ?](#wha-)
2. [Coms](#coms)
   1. [Scaffold Setup](#scaffold-setup)
   2. [Site Deployed](#site-deployed)

---

# Wha ?

Learning and testing deployment of [`https://docusaurus.io/`](https://docusaurus.io/)

- This is by FB

# Coms

## Scaffold Setup 

```sh 
npx create-docusaurus@latest my-website classic --typescript
```

## Site Deployed

1. While deploying on vercel choose `Docusaurus 2`


$$\huge \color{pink}{https://sn-dokusa-1.vercel.app/}$$

[`https://sn-dokusa-1.vercel.app/`](https://sn-dokusa-1.vercel.app/)
- deployed